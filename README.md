Image for running PHP command line commands
===========================================

Includes wp-cli and stuff required by it.

Building
--------

Building Docker image:

```bash
docker build -t registry.gitlab.com/wysiwygoy/php-cli-wp .
```

Running WP-CLI
--------------

```bash
docker run registry.gitlab.com/wysiwygoy/dev/php-cli-wp wp
```
