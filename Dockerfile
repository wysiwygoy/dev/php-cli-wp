#
# PHP-CLI container configured for WordPress
#

FROM php:alpine
MAINTAINER Jarno Antikainen <jarno.antikainen@wysiwyg.fi>

LABEL Description="This image is used for running PHP on command line." Vendor="Wysiwyg Oy"

COPY wait-for-it.sh /usr/local/bin/

RUN chmod a+x /usr/local/bin/wait-for-it.sh \
    && docker-php-ext-install mysqli \
    && curl https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar --output /usr/local/bin/wp \
	&& chmod a+x /usr/local/bin/wp \
    && apk add --no-cache bash less mariadb-client openssh-client

USER www-data
VOLUME /project
WORKDIR /project
